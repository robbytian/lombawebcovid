//navbar ketika scroll
const nav = document.querySelector('nav');

window.addEventListener('scroll', function() {
  const offset = window.pageYOffset;
  
  if(offset > 75)
    nav.classList.add('scroll')
  else 
    nav.classList.remove('scroll')
});


//gambar slider
var slideIndex = 0;
showSlides();

function showSlides() {
  var i;
  var slides = document.getElementsByClassName("image_slider");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1;}    

  slides[slideIndex-1].style.display = "block";  
  setTimeout(showSlides, 5000);
  // Change image every 2 seconds
}

//responsive navbar
function navResponsive() {
    var x = document.getElementById('navbar');
    if (x.className === "nav") {
        x.className += " responsive";
    }
    else{
        x.className = "nav";
    }
    
  } 
